#include <stdio.h>
void printBytes(unsigned char *start, int len) {
	for (int i = 0; i < len; ++i) {
		 printf(" %.2x", start[i]);
 		}
 	printf("\n");
}
void printInt(int x) {
	printBytes((unsigned char *) &x, sizeof(int));
}
void printFloat(float x) {
	printBytes((unsigned char *) &x, sizeof(float));
}
void printShort(short x) {
	printBytes((unsigned char *) &x, sizeof(short));
}
void printLong(long x) {
	printBytes((unsigned char *) &x, sizeof(long));
}
void printDouble(short x) {
	printBytes((unsigned char *) &x, sizeof(double));
}


int main()
{
   // Calls replace method with examples presented in the assignment
    printInt(45);
    //the bytes are left to right order -
    //this is due to little endian
    //2d 00 00 00 instead of 00 00 00 2d
    printFloat(45.0f);
    //bytes have weird stuff at end due to representation (aliasing?)

    printShort(45);
    //bytes are in left to right order - little endian
    printLong(45.0l);
    //bytes are in left to right order - little endian
    printDouble(45.0);
    //  bytes have weird stuff at end due to decimal point


   return 0;
}
